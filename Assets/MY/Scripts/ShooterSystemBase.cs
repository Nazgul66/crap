﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterSystemBase : MonoBehaviour {
    [SerializeField]
    GameObject projectile;

    [SerializeField]
    float speed;


    private void FixedUpdate() {
        if (Input.GetButton("Fire1")) {
            var clone = Instantiate(projectile, transform.position, transform.rotation);
            clone.GetComponent<Rigidbody>().velocity = transform.TransformDirection(new Vector3(0, 0, speed)); //AddForce(new Vector3(0, 0, speed), ForceMode.Force);
            Destroy(clone.gameObject, 3);
        }
    }
}
