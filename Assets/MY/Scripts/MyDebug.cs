﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyDebug : MonoBehaviour {
    public bool show;

    private void OnTriggerEnter(Collider other) {
        if (show) {
            Debug.Log(gameObject.name);
            Debug.Log(other.gameObject.name);
        }
    }
}
